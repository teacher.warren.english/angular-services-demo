# NgWeek4

Angular demonstration application, featuring Angular services and security integration with Keycloak.

## Instructions

Run `npm install` to install all of the necessary dependencies.
Make sure if you are running from the `auth` branch that you have an instance of KeyCloak running, with the appopriate configuration inside the `src/assets/keycloak.json` file.
Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Branches
On the `main` branch is only the Angular services code.
On the `auth` branch is the security source code, including some extra features like routing.

## Contributors
Me - Warren West (Noroff Accelerate)
&copy; 😎