import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, retry } from 'rxjs';
import { UserDto } from '../models/userDto';
import { RootObject, UserResponse } from '../models/userResponse';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private readonly http: HttpClient) { }

  // default currentUser state 
  private _currentUser: UserDto = {
    name: 'Warren West',
    username: 'w_west',
    email: 'warren@west.com',
    picture: 'warren.jpg'
  }

  public get currentUser(): UserDto {
    return this._currentUser
  }

  login(): void {
    // retrieve a user
    this.http.get<RootObject>('https://randomuser.me/api')
      .pipe(
        map((response) => {
          let parsedUser: UserDto = {
            name: `${response.results[0].name.first} ${response.results[0].name.last}`,
            username: response.results[0].login.username,
            email: response.results[0].email,
            picture: response.results[0].picture.medium
          }
          return parsedUser
        }),
        retry(2),
        // chain on as many RxJS operator as you'd like
      )
      .subscribe({
        next: (parsedUser) => {
          console.log(parsedUser); // this is already JSON
          this._currentUser = parsedUser
        },
        error: (error) => { console.error(error.message) }
      })
  }

  private loginAPICall(): Observable<RootObject> {
    // retrieve a user
    return this.http.get<RootObject>('https://randomuser.me/api')
  }

  loginTwo(): UserDto {
    let result: UserDto = {
      name: '',
      username: '',
      email: '',
      picture: ''
    }

    this.loginAPICall()
      .pipe(
        map((response) => {
          let parsedUser: UserDto = {
            name: `${response.results[0].name.first} ${response.results[0].name.last}`,
            username: response.results[0].login.username,
            email: response.results[0].email,
            picture: response.results[0].picture.medium
          }
          return parsedUser
        })
      )
      .subscribe({
        next: (parsedUser) => {
          console.log(parsedUser); // this is already JSON
          // this._currentUser = 
          result = parsedUser
        },
        error: (error) => { console.error(error.message) }
      })
      
      // the result returned here is still the empty, initialized "result" object
      // the observable is only assigned to result when the API call responds successfully
      // so here we hit a synchronisity issue.
      // This is why we don't usually try to force returning an object, and instead work with Observables that update state.
      // Our pieces of state that update will automatically cause re-rendering when necessary 
      return result
  }
}
