import { Component, Input } from '@angular/core';
import { UserDto } from 'src/app/models/userDto';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-content',
  templateUrl: './user-content.component.html',
  styleUrls: ['./user-content.component.css']
})
export class UserContentComponent {

  constructor(private readonly userService: UserService) {}
  localStateUser = { name: ''}

  @Input() messageFromParent : string = ''

  public get currentUser() : UserDto {
    return this.userService.currentUser
  }

  // The handler that calls the service's loginTwo function
  // This does not update the UI.
  handleGetUserTwo(): void {
    let myUser = this.userService.loginTwo()
    this.localStateUser = myUser
    console.log(myUser);
  }

  handleGetUser(): void {
    this.userService.login()
  }
}
