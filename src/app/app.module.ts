import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { UserComponent } from './pages/user/user.component';
import { UserContentComponent } from './components/user-content/user-content.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserContentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
