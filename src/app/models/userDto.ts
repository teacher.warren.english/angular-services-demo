export interface UserDto {
    name: string;
    username:string;
    email: string;
    picture: string;
}